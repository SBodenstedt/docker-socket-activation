#!/bin/bash

/usr/bin/python3 /etc/manage-docker/extract_services.py /etc/manage-docker/Manage_Docker.ini
cd /etc/systemd/system/
for i in *proxy.socket; do 
	/bin/systemctl enable $i;
	/bin/systemctl start $i;
done

