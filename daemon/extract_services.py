import configparser
import os
import subprocess
import threading
import datetime
import time
import sys

config = configparser.ConfigParser()
config.read(sys.argv[1])

for sec in config:
    #print(sec)
    if sec == 'DEFAULT':
        continue
    
    main_container = config[sec]["main_container"]
    host_ports = config[sec]["host_ports"].split(" ")
    container_ports = config[sec]["container_ports"].split(" ")

    containers = main_container
    if "additional_containers" in config[sec]:
        containers += " " + config[sec]["additional_containers"]

    f = open("/etc/systemd/system/" + sec + "-docker.service", "w")
    f.write("[Unit]\n")
    f.write("Description=" + sec + " container\n\n")
    f.write("[Service]\n")
    f.write("ExecStart=/usr/bin/python3 /etc/manage-docker/manage_docker.py client unpause " + sec + "\n")
    f.write("ExecStartPost=/bin/sleep 1\n")
    #f.write("ExecStop=/usr/bin/python3 /etc/manage-docker/manage_docker.py client " + sec + "\n")
    f.close

    for i in range(len(host_ports)):
        f = open("/etc/systemd/system/" + sec + "_" + host_ports[i] + "-proxy.service", "w")
        f.write("[Unit]\n")
        f.write("Requires=" +  sec + "-docker.service\n")
        f.write("After=" +  sec + "-docker.service\n\n")
        f.write("[Service]\n")
        f.write("Type=oneshot\n")
        f.write("ExecStart=/lib/systemd/systemd-socket-proxyd --exit-idle-time=5min " + main_container + ":" + container_ports[i] + "\n")
        f.write("ExecStop=/usr/bin/python3 /etc/manage-docker/manage_docker.py client pause " + sec + "\n")
        f.close

        f = open("/etc/systemd/system/" + sec + "_" + host_ports[i] + "-proxy.socket", "w")
        f.write("[Socket]\n")
        f.write("ListenStream=" + host_ports[i] + "\n\n")
        f.write("[Install]\nWantedBy=sockets.target\n")
        f.close

        #time.sleep(10)
        #print(subprocess.run(["systemctl", "daemon-reload"]))
        #print(subprocess.run(["systemctl", "unmask", sec + "_" + host_ports[i] + "-proxy.socket"]))
        #print(subprocess.run(["systemctl", "enable", sec + "_" + host_ports[i] + "-proxy.socket"]))

        #print(subprocess.run(["systemctl", "list-unit-files"]))

    

    #print(subprocess.run(["systemctl", "enable", sec + "-docker.service"]))

   