from multiprocessing.connection import Listener, Client
import sys


if __name__ == '__main__':
    address = ('localhost', 6000)
    authkey = b"ManageDocker"
    if sys.argv[1] == "server":
        import configparser
        import os
        import subprocess
        import threading
        import datetime
        import time

        try:
            os.symlink("/docker.sock", "/var/run/docker.sock")
        except:
            print("Symlink already exists")

        containers = {}
        states = {}
        config = configparser.ConfigParser()
        config.read(sys.argv[2])
        print(subprocess.run(["docker", "network", "rm", "manage_docker_net"]))
        print(subprocess.run(["docker", "network", "create", "manage_docker_net"]))
        print(subprocess.run(["docker", "network", "connect", "manage_docker_net", "manage_docker"]))
        for sec in config:
            #print(sec)
            if sec == 'DEFAULT':
                continue
            
            main_container = config[sec]["main_container"]
            network = subprocess.check_output(["docker", "inspect", main_container, "--format='{{ .HostConfig.NetworkMode }}'"], text=True)
            network = network[1:-2]
            #print(subprocess.run(["docker", "network", "connect", network, "manage_docker"]))
            print(subprocess.run(["docker", "network", "connect", "--alias", main_container, "manage_docker_net", main_container]))
            containers[sec] = main_container
            states[sec] = datetime.datetime.now() + datetime.timedelta(minutes=15)
            if "additional_containers" in config[sec]:
                containers[sec] += " " + config[sec]["additional_containers"]
            containers[sec] = containers[sec].split(" ")
        print(containers)

        lock = threading.Lock()

        def time_thread():
            print("start thread")
            while True:
                lock.acquire()
                for container in states:
                    if states[container] == 0:
                        continue
                    elif states[container] < datetime.datetime.now():
                        states[container] = 0
                        print(subprocess.run(["docker", "pause"] + containers[container]))
                        print("Pausing ", container)
                lock.release()
                #time.sleep(5*60)
                time.sleep(5)
                
        listener = Listener(address, authkey=authkey)
        thread = threading.Thread(target=time_thread)
        thread.start()

        while True:
            conn = listener.accept()
            data_in = conn.recv()
            conn.close()
            data_in = data_in.split(" ")
            action = data_in[0]
            container_id = data_in[1]

            if action == "unpause":
                print(subprocess.run(["docker", "unpause"] + containers[container_id]))
                lock.acquire()
                states[container_id] = 0
                lock.release()
            else:
                lock.acquire()
                #states[container_id] = datetime.datetime.now() + datetime.timedelta(minutes=15)
                states[container_id] = datetime.datetime.now() + datetime.timedelta(seconds=15)
                lock.release()
                print("Updating ", container_id)

    else:
        f = open("/var/log/manager-docker.log","a")
        f.write("Start client\n")
        f.close()
        conn = Client(address, authkey=authkey)
        conn.send(sys.argv[2] + " " + sys.argv[3])
        conn.close
