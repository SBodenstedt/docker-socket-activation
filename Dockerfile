#FROM ubuntu:latest
FROM ubuntu:20.10

RUN export DEBIAN_FRONTEND=noninteractive;apt-get update; apt-get install -y docker.io systemd-sysv; \
(cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done);\
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*;\
rm -f /lib/systemd/system/sockets.target.wants/*udev*;\
rm -f /lib/systemd/system/sockets.target.wants/*initctl*;\
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

VOLUME [ "/sys/fs/cgroup" ]

COPY ManageServer.service  /etc/systemd/system/
COPY prepare.service /etc/systemd/system/

RUN systemctl enable ManageServer.service
RUN systemctl enable prepare.service
#CMD ["/usr/bin/python3 /etc/manage-docker/extract_services.py /etc/manage-docker/Manage_Docker.ini;for i in /etc/systemd/system/*proxy.socket; do /bin/systemctl enable $i;done;/sbin/init"]
#CMD ["/etc/manage-docker/prepare.sh"]
#CMD ["/bin/ls /etc/"]
CMD ["/sbin/init"]
