Docker Socket Activition
----
The repo contains a Docker container, based on Ubuntu 20.10 and Systemd, I wrote that pauses/unpauses other Docker containers (as defined in `daemon/Manage_Docker.ini`) on the host based on port activity/inactivity. I am using it to reduce disk usage on a NAS on inactivity.

To use the container, all you have to do is to fill out your own containers in `daemon/Manage_Docker.ini` and add your port mapping to the `docker-compose.yml`. For example, if you have a container _container1_ that listens internally on ports 80 and 22 for connections and you want to map them to ports 8080 and 2222 on the host and furthermore _container1_ also depends on _container2_, you have to add the following to `daemon/Manage_Docker.ini`:
```
[example_name]
main_container = container1
additional_containers = container2
host_ports = 8080 2222
container_ports = 80 22
```
and add the following to `docker-compose.yml`:
```
    ports:
        - "8080:8080/tcp"
        - "2222:2222/tcp"
```
Here it is important to note that _container1_ isn't already exposing ports 80 and 22 to host ports 8080 and 2222, otherwise you should select other ports here.

Run the following to start the Docker:
```
    docker-compose build
    docker-compose up -d
```

Once a client has connect to the port on the managing container _container1_  will be unpaused and traffic will be forwarded to it. If the connection is idle for 5 minutes, the forward is stopped and *container1* is paused again.

The article [here](https://blog.developer.atlassian.com/docker-systemd-socket-activation/) served as source and inspiration for the sockets/service files in this project.
